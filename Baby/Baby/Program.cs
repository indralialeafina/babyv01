﻿using System;
using System.Threading;

namespace Baby
{
    enum GameStatus {MainMenu,isRunning,isOver}
    internal class Program
    {
        public static void Main(string[] args)
        {
            GameStatus status = GameStatus.MainMenu;
            // demander le nom et sexe du bébé
            Baby baby = GetPlayerInput();
            
            Console.Clear();
            Console.WriteLine(baby);          
            // being game loop
            int hours = 0;
            
            status = GameStatus.isRunning;
            while (status == GameStatus.isRunning)
            {

                PrintInstructions();
                GetPlayerChoice(baby);
                Thread.Sleep(2000);
                //Console.ReadKey();
                baby.PassTime(1);
                hours += 1;
                Console.WriteLine("Vous avez le bébé depuis " + hours + " heure" + (hours == 1 ? "" : "s"));
                Console.WriteLine("Le bébé est " + baby.HowIsBaby());
                if (baby.HowIsBaby() != "OK")
                {
                    status = GameStatus.isOver;
                }
                //Console.WriteLine(baby);
                
            }
            Console.WriteLine("Fin du jeu, vous avez duré pendant " + hours + " heures !!");
            
            // print instructions
            //
            /*Baby babyBoy = new Baby("Alex",'M',random);
            Console.Write(babyBoy);
            Baby babyGirl = new Baby("Lea Rose", 'F',random);
            Console.Write(babyGirl);
            babyBoy.FeedBaby(-10,10);
            Console.Write(babyBoy);*/

        }


        public static void GetPlayerChoice(Baby baby)
        {
            char choice = ' ';
            do
            {
                Console. Write("Faites votre choix :");
                char.TryParse(Console.ReadLine(), out choice);
            } while (char.ToUpper(choice) != 'N' && char.ToUpper(choice) != 'B' && char.ToUpper(choice) != 'D' && char.ToUpper(choice) != 'C' && char.ToUpper(choice) != 'A');

            switch (char.ToUpper(choice))
            {
                case 'N' :
                    Console.WriteLine("Vous nourrissez le bébé...");
                    baby.FeedBaby(25);
                    break;
                case 'B' :
                    Console.WriteLine("Vous faites boire le bébé...");
                    baby.FeedBaby(0,25);
                    break;
                case 'D' :
                    Console.WriteLine("Vous faites dormir le bébé...");
                    baby.LayDownBaby(5);
                    break;
                case 'C' :
                    Console.WriteLine("Vous changez la couche du bébé...");
                    baby.ChangeDiaper(25);
                    break;
                case 'A' :
                    Console.WriteLine("Vous amusez le bébé...");
                    baby.EntertainBaby(4);
                    break;
                default:
                    break;
            }
        }
        public static void PrintInstructions()
        {
            Console. WriteLine("N pour Nourrir");
            Console. WriteLine("B pour Biberon");
            Console. WriteLine("C pour Changer Couche");
            Console. WriteLine("D pour Dormir");
            Console. WriteLine("A pour Amuser");
        }

        public static Baby GetPlayerInput()
        {
            Console.WriteLine("Bienvenue au jeu du bébé");
            Console.Write("Veuillez entrer un nom : ");
            string name = Console.ReadLine();
            char sex = ' ';
            do
            {
                Console.Clear();
                Console.Write("Merci, veillez entrer le sexe de " + name + " ( M ou F ) : ");
                char.TryParse(Console.ReadLine(), out sex);
            } while (char.ToUpper(sex) != 'F' && char.ToUpper(sex) != 'M');

            Baby baby = new Baby(name, sex, new Random());
            return baby;
            //Console.WriteLine("Here we are");
        }
    }
}