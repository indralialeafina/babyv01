using System;

namespace Baby
{
    public class Baby
    {
        
        private string Name { get; set; }
        private char Sex { get; set; }
        private int Hunger { get; set; }
        private int HydrationLevel { get; set; }
        private int DiaperLevel { get; set; }
        private int SleepLevel { get; set; }
        private int AmusementLevel { get; set; }
    
        public Baby(string name, char sex, Random random)
        {
            
            Name = name;
            Sex = Char.ToUpper(sex);
            Hunger = random.Next(0, 101);
            HydrationLevel = random.Next(0, 101);
            DiaperLevel = random.Next(0, 101);
            SleepLevel = random.Next(0, 101);
            AmusementLevel = random.Next(0, 101);
        }

        /*public void ShowBabyGraph()
        {
            /*Console.WriteLine("{0,10}{1,10}{2,10}{3,10}{4,10}",
            customer[DisplayPos],
            sales_figures[DisplayPos],
            fee_payable[DisplayPos], 
            seventy_percent_value,
            thirty_percent_value);#1#
            
           // Console.Clear();
            // set titles at the bottom 11 spaces below the first line
           Console.SetCursorPosition(0, 11);
            Console.WriteLine("{0,10}{1,10}{2,10}{3,10}{4,10}", "Food", "Water", "Diaper", "Sleep", "Fun");
            string hungerValue = "";
            string waterValue = "";
            string diaperValue = "";
            string sleepValue = "";
            string funValue = "";
            int percentile = 10;
            for (int i = 10; i > 0; i--)
            {
                //Console.SetCursorPosition(0,i+1);
                if ((Hunger <= percentile * i) && (Hunger > percentile * i - 1)) 
                {
                    
                    hungerValue = "X";
                }
                else
                {
                    hungerValue = " ";
                }
                Console.WriteLine("{0,10}{1,10}{2,10}{3,10}{4,10}", hungerValue, "Water", "Diaper", "Sleep", "Fun");

                
            }
            // puts x's at the right places for every line afterwards

        }*/
        private string LevelStatus(int level)
        {
            if (level == 0)
                return "immediatement";
            if (level >= 1 && level <=25)
                return "extremement";
            if (level > 25 && level <= 50)
                return "beaucoup";
            if (level > 50 && level < 75)
                return "moyennement";
            if (level > 75 && level < 100)
                return "un peu";
            if (level == 100)
                return "aucunement";
            return "";
        }

        public void FeedBaby(int foodAmount=0, int waterAmount = 0)
        {
            if (Hunger + foodAmount >= 100)
            {
                Hunger = 100;
            }
            else
            {
                Hunger += foodAmount;
            }

            if (HydrationLevel + waterAmount >= 100)
            {
                HydrationLevel = 100;
            }
            else
            {
                HydrationLevel += waterAmount;
            }

        }
        
        public void ChangeDiaper(int minutesSpent)
        {
            if (DiaperLevel + minutesSpent >= 100)
            {
                DiaperLevel = 100;
            }
            else
            {
                DiaperLevel += minutesSpent;
            }
        }

        public void LayDownBaby(int hourOfSleep)
        {
            if (SleepLevel + hourOfSleep * 10 >= 100)
                SleepLevel = 100;
            else
            {
                SleepLevel += hourOfSleep * 10;
            }
        }

        public void EntertainBaby(int hoursOfFun)
        {
            if (AmusementLevel + hoursOfFun * 10 > 100)
                AmusementLevel = 100;
            else
            {
                AmusementLevel += hoursOfFun * 10;
            }
        }
        public void PassTime(int hours)
        {
            Console.Clear();
            Console.WriteLine(hours + " heure" + (hours>1?"s ont":" a ") + " passé ");
            FeedBaby(-5*hours,-5*hours);
            ChangeDiaper(-5*hours/5);
            LayDownBaby(-1);
            ChangeDiaper(-5);
            EntertainBaby(-hours/3);
            Console.WriteLine(this);
        }

        public string HowIsBaby ()
        {
            if (Hunger <= 0)
            {
                return "mort de faim !!";
            }

            if (HydrationLevel <= 0)
            {
                return "mort de soif !!";
            }

            if (DiaperLevel <= 0)
            {
                return "mort d'une bacterie fecale !!";
            }

            if (SleepLevel <= 0)
            {
                return "mort de fatigue !!";
            }

            if (AmusementLevel <= 0)
            {
                return "mort d'ennui !!";
            }
            return "OK";
        }


        public override string ToString()
    {
        string pronom = (Sex == 'M' ? "Il " : "Elle ");
            string description = $"Le bebe nommé : {Name} est un" + (Sex=='F'?"e ":" ") + (Sex=='M'?"garçon": "fille") + "\n" + 
                    pronom + "a " + LevelStatus(Hunger) + $" faim ({Hunger}/100)\n" +
                    pronom + "a " + LevelStatus(HydrationLevel) + $" soif ({HydrationLevel}/100)\n" +
                    pronom + "a une couche " + LevelStatus(DiaperLevel) + $" pleine ({DiaperLevel}/100)\n"+
                    pronom + "a " + LevelStatus(SleepLevel) + $" besoin de sommeil ({SleepLevel}/100)\n" +
                    pronom + "a " + LevelStatus(AmusementLevel) + $" besoin de s'amuser ({AmusementLevel}/100)\n-------------";
            return description;
        }
    }
    
    

}